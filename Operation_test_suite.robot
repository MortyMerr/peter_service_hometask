*** Settings ***
Library  Test
Documentation  Tests for basic functionality of ExpressionConverter.getExperessionResult.
Test Template  Basic operations should be correct

#I looked at the code, I know some inf about method.
#However, it seems to me, that I should test this function as a black box, ignoring information about realisation.
#But if i am right, i should have some specification. So, let's imagine, that u wrote me in email, that function returns
#"There is no statement : " + expr in case, when we pass bad_expr. Bad_expr = any not correct math expression

*** Test Cases ***
Single number               2               2

Simple add                  2 + 2           4
Simple sub                  4 - 2           2
Simple mul                  2 * 2           4
Simppe div                  4 / 2           2

Floating point              1e-2 + 1e-2     2e-2
Operation with fractions    0.5 + 0.5       1
Fraction answer             1 / 2           0.5

Negative answer             2 - 4           -2
Unary plus                  + 2             2
Unary minus                 - 2             -2
Operation with unary minus  2 - -2          4

Unary plus expr             +2 + 2          4
Unary minus expr            -2 + 2          0
Double unary minus          --2             2

More than one operators     2 + 2 + 2       6
Brackets                    (2 + 2)         4

Unary minus with brackets   -(2 + 2)        -4

Accuracy                    0.0000000000000001 + 1   1.0000000000000001
Big numbers                 1e100 + 1e100            2e100


*** Keywords ***
Basic operations should be correct
    [Arguments]  ${expr}  ${answer}
    ${funcAnswer} =  Test.Get Expression Result  ${expr}
    Should be Equal As Numbers  ${funcAnswer}  ${answer}
