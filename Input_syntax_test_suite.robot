*** Settings ***
Library  Test
Documentation  Tests for error input of ExpressionConverter.getExperessionResult.
Test Template  Excpetion should be handled

*** Variables ***
${no_statement} =  There is no statement :

*** Test Cases ***
Passing char    Hello       ${no_statement} Hello
Passing char with operator
                2/2Hello    ${no_statement} 2/2Hello



*** Keywords ***
Excpetion should be handled
    [Arguments]  ${expr}  ${answer}
    ${funcAnswer} =  Test.Get Expression Result  ${expr}
    Should be Equal  ${funcAnswer}  ${answer}
